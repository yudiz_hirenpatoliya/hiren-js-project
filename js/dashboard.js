(function () {
    if (!localStorage.getItem("email")) {
        window.location.href = "login.html";
    }
})();

function logoutuser() {
    localStorage.removeItem('email');
    window.location.href = "login.html";
}

function weatherApi() {
    let lat;
    let long;
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                lat = position.coords.latitude;
                long = position.coords.longitude;
                fetch(
                    `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&units=metric&appid=ac5d68ef6e7594df77ca70cc0fa69424`
                )
                    .then((response) => response.json())
                    .then((data) => {
                        const name = data.name;
                        const description = data.weather[0].description;
                        const temp = data.main.temp;
                        const speed = data.wind.speed;

                        document.querySelector(".city").innerHTML ="Weather Of " + name;
                        document.querySelector(".temp").innerHTML = temp + "°C";
                        document.querySelector(".desc").innerHTML = description;
                        document.querySelector(".wind").innerHTML ="Wind: " + speed + " KMPH";
                    })
                    .catch((error) => console.log(error));
            });
        }
    }
    getLocation();
}
weatherApi();

// GIF
function gifApi() {
    fetch(
        `https://api.giphy.com/v1/gifs/random?api_key=w5kVyG3lzcm2yQNbIHsIImohiaZCklHH`
    )
        .then((response) => response.json())
        .then((data) => showGif(data.data.images.downsized_medium.url));
}
function showGif(url) {
    const gifDiv = document.querySelector(".gif");

    gifDiv.innerHTML = `<img src="${url}" style="width: 180px;" alt="gif">`;
}
gifApi();
setInterval(() => {
    gifApi();
}, 20000);

// Time and Date
setInterval(() => {
    var today = new Date();
    var date =
        today.getFullYear() +"/" +(today.getMonth() + 1) + "/" + today.getDate();
    var time =
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    const datehtml = document.getElementById("date");
    const timehtml = document.getElementById("time");

    datehtml.innerHTML = `${date}`;
    timehtml.innerHTML = `${time}`;
}, 1000);

//todo
let addbtn = document.getElementById('addBtn');
let todo = document.getElementById('todoform');
let ul = document.getElementById('todoitem');
let email = localStorage.getItem('email');

window.addEventListener('load', (event) => {
    showData();
});

addbtn.addEventListener("click", (eve) => {
    
        eve.preventDefault();
        addData();
        function addData() {
            let input = document.getElementById('createtask').value;
            if (input == '') {
                alert('Please Enter Task 🐧');
            } else {
                if (!localStorage.getItem(email)) {
                    let arr = [];
                    let todo = { id: Date.now(), taskname: input, completed : false };
                    arr.push(todo);
                    localStorage.setItem(email, JSON.stringify(arr));
                    alert('Task Added');
                } else {
                    let arr = JSON.parse(localStorage.getItem(email));
                    arr.push({ id: Date.now(), taskname: input, completed : false});
                    localStorage.setItem(email, JSON.stringify(arr));
                    alert('Task Added');
                }
                document.getElementById('createtask').value = "";
                showData();
            }
        }
    

})

function showData() {
    let arr = JSON.parse(localStorage.getItem(email));
    if (arr !== null) {
        let checked = arr.filter((ele) => {
            return ele.completed == true;
        })
        let unchecked = arr.filter((ele) => {
            return ele.completed == false;
        })

        document.getElementById('todoitem').innerHTML = "";
        document.getElementById('completeitem').innerHTML = "";
        unchecked.map((item) => {
            let li = document.createElement('li');
            li.classList.add('li');
            li.innerHTML = `<div class="main"><div class="d">${item.taskname}</div><div class="btns"><input type='checkbox' name='checkbox' class="check_Btn" id=${item.id}><button id=${item.id} class="edit_Btn" name="editBtn">Edit</button> <button id=${item.id} class="del_Btn" name="delBtn">Delete</button></div></div>`;
            li.id = item.id;
            document.getElementById('todoitem').appendChild(li);
        })

        checked.map((item)=>{
            let li = document.createElement('li');
            li.classList.add('li');
            li.innerHTML = `<div class="main"><div class="d">${item.taskname}</div><div class="btns"><input type='checkbox' checked name='checkbox' class="check_Btn" id=${item.id}><button id=${item.id} class="edit_Btn" name="editBtn">Edit</button> <button id=${item.id} class="del_Btn" name="delBtn">Delete</button></div></div>`;
            li.id = item.id;
            document.getElementById('completeitem').appendChild(li);
        })
    }
}


let edit = document.getElementById('todoitem');
edit.addEventListener("click", (e) => {
    let targetId = e.target.id;
    let input = document.getElementById('createtask');
    if (e.target.name == 'editBtn') {
        let arr = JSON.parse(localStorage.getItem(email));
        arr = arr.forEach((item) => {
            if (item.id == targetId) {
                input.value = item.taskname;
                let addbtn = document.getElementById('addBtn');
                let btn = document.createElement('button');
                btn.classList.add('common-btn');
                btn.innerText = "Update";
                // console.log(todo.childNodes[3].innerText);
                if(todo.childNodes[3].innerText){
                    todo.appendChild(btn);
                    todo.removeChild(addbtn);
                }
                btn.addEventListener("click",(e)=>{
                    e.preventDefault();
                    let arr = JSON.parse(localStorage.getItem(email));
                    arr = arr.map((element)=>{
                        if(element.id == targetId){
                            return {...element,taskname:input.value}
                        }
                        return element;
                    })
                    localStorage.setItem(email, JSON.stringify(arr));
                    alert("Task Updated");
                    todo.appendChild(addbtn);
                    todo.removeChild(btn);
                    (function(){
                        window.location.reload();
                    })()
                    input.value = "";
                    showData();
                })              
            }
        })
    }
    if(e.target.name == 'delBtn')
    {
        let arr = JSON.parse(localStorage.getItem(email));
         arr.forEach((item,index)=>{
            if(item.id == targetId)
            {
                arr.splice(index,1); 
            }
        })
        localStorage.setItem(email, JSON.stringify(arr));
        alert('Task deleted');
        showData();
    }
    if(e.target.name == 'checkbox')
    {
        let arr = JSON.parse(localStorage.getItem(email));
        let targetId = e.target.id;
        arr = arr.map((element)=>{
            if(element.id == targetId)
            { 
                if(element.completed == false){
                    return { ...element, completed: true } 
                }  
                if(element.completed == true){
                    return { ...element, completed: false }
                }  
            }
            return element;
             
        })
        localStorage.setItem(email, JSON.stringify(arr));
        showData();
    }
})

document.getElementById('completeitem').addEventListener("click", (e) => {
    let targetId = e.target.id;
    if(e.target.name == 'delBtn')
    {
        let arr = JSON.parse(localStorage.getItem(email));
         arr.forEach((item,index)=>{
            if(item.id == targetId)
            {
                arr.splice(index,1); 
            }
        })
        localStorage.setItem(email, JSON.stringify(arr));
        alert('Task deleted');
        showData();
    }
    if(e.target.name == 'checkbox')
    {
        let arr = JSON.parse(localStorage.getItem(email));
        let targetId = e.target.id;
        arr = arr.map((element)=>{
            if(element.id == targetId)
            { 
                if(element.completed == false){
                    return { ...element, completed: true }    
                }  
                if(element.completed == true){
                    return { ...element, completed: false }
                }  
            }
            return element;
             
        })
        localStorage.setItem(email, JSON.stringify(arr));
        showData();
    }
})

 


