let _name = document.getElementById('name')
let email = document.getElementById('email');
let mobile = document.getElementById('mob');
let password = document.getElementById('pass');

function dispField(){
    let getUser = localStorage.getItem("email");
    let users = JSON.parse(localStorage.getItem("userRecord"));
    let chk = users.find((item)=>{
        return item.email === getUser;
    })
    
     _name.value = chk._name;
     email.value = chk.email;
     mobile.value = chk.mobile;
     password.value = chk.pass;
}
dispField();

function update(){
    let getUser = localStorage.getItem("email");
    let users = JSON.parse(localStorage.getItem("userRecord"));
    let chk = users.find((item)=>{
        return item.email === getUser;
    })

    users.forEach((user,index)=> {
        if(user.email == localStorage.getItem('email'))
        {
            users[index]._name = _name.value;
            users[index].email = email.value;
            users[index].mobile = mobile.value;
            users[index].pass = password.value;
        }
    });

    localStorage.setItem('userRecord',JSON.stringify(users));
    localStorage.setItem('email',chk.email);
    alert('Profile updated');
    window.location.href = "../profile.html";
}
