let lid = document.getElementById('loginform');

lid.addEventListener("submit",(eve)=>{
    eve.preventDefault();
    let email = document.getElementById('email');
    let pass = document.getElementById('pass');

    let users = JSON.parse(localStorage.getItem("userRecord"));

    let chk = users.find((item)=>{
        return item.email === email.value && item.pass === pass.value;
    })
    
    if(chk && users!== null)
    {
        alert("Login success");
        localStorage.setItem("email",email.value);
        window.location.href="Dashboard.html";
    } else {
        alert("Your email and password don't match. Please try again");
        window.location.href="login.html";
    }
})

