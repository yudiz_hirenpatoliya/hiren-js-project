
let fid = document.getElementById('regform');

fid.addEventListener("submit", (eve) => {
    eve.preventDefault();
    let _name = document.getElementById('name');
    let email = document.getElementById('email');
    let mobile = document.getElementById('mob');
    let gen = document.getElementById('gender');
    let dob = document.getElementById('dob');
    let pass = document.getElementById('pass');

    function validateName(_name) {
        let chkname = _name.trim();
        if (chkname.length <3) {
            alert("Name should be 3 Characters");
            clearForm();
            return false;
        } else {
            return true;
        }
    }

    function validateMob(mobile) {
        if (mobile.length != 10) {
            alert("Mobile no. must be 10 digits");
            clearForm();
            return false;
        } else {
            return true;
        }
    }

    function validateDob(dob) {
        let currdate = new Date();
        currdate.setHours(0, 0, 0, 0);
        let bdate = new Date(dob);
        if (bdate <= currdate) {
            return true;
        } else {
            alert("Invalide Date");
            clearForm();
            return false;
        }
    }

    function validatePass(pass) {
        if (pass.length<6) {
            alert("Password must contain an letter and numbers 6 Minimum Character long");
            clearForm();
            return false;
        } else {
            return true;
        }
    }

    function Reg(_name, email, mobile, gen, dob, pass) {
        let user = localStorage.getItem('userRecord');
        if (user) {
            user = JSON.parse(user);
            user.push({ _name, email, mobile, gen, dob, pass });
            localStorage.setItem('userRecord', JSON.stringify(user));
        } else {
            let anotherarr = [];
            anotherarr.push({ _name, email, mobile, gen, dob, pass });
            localStorage.setItem('userRecord', JSON.stringify(anotherarr))
        }
    }

    if (validateName(_name.value) && validateMob(mobile.value) && validateDob(dob.value) && validatePass(pass.value)) {
        Reg(_name.value, email.value, mobile.value, gen.value, dob.value, pass.value);
        alert("User Registered Successfully!!");
        clearForm();
        document.location.href = "login.html";
    } else {
        document.location.href = "register.html";
    }

    function clearForm() {
        _name.value = "";
        email.value = "";
        mobile.value = "";
        gen.value = "";
        dob.value = "";
        pass.value = "";
    }

})


