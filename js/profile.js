(function () {
    if (!localStorage.getItem("email")) {
        window.location.href = "login.html";
    }
})();
function displayProfileData() {
    let name, email, mobile, gender, dob;

    name = document.getElementById("name");
    email = document.getElementById("email");
    mobile = document.getElementById("mobile");
    dob = document.getElementById("dob");

    let userEmail = localStorage.getItem("email");

    let users = JSON.parse(localStorage.getItem("userRecord"));

    let loggedinUser = users.find((item) => {
        return item.email == userEmail;
    });

    name.innerHTML = "Name: "+loggedinUser._name;
    email.innerHTML = "Email: "+loggedinUser.email;
    mobile.innerHTML = "Mobile No.: "+loggedinUser.mobile;
    dob.innerHTML = "Dob: "+loggedinUser.dob;
}
displayProfileData();
function logoutuser(){
    localStorage.removeItem('email');
    window.location.href="login.html";
}

